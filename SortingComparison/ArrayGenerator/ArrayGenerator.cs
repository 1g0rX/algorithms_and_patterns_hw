﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayGenerator
{
    public enum ArrayType
    {
        Random,
        Sorted,
        PartialSorted
    }

    public class UnexpectedEnumValueException<T> : Exception
    {
        public UnexpectedEnumValueException(T value)
            : base("Value " + value + " of enum " + typeof(T).Name + " is not supported")
        {
        }
    }
    public static class GenerateArray
    {
        static int[] array;

        public static int[] Get(ArrayType type, int length = 100, int minVal = 0, int maxVal = 100)
        {
            switch(type)
            {
                case ArrayType.Random: GenerateFactory(new RandomArrayFactory(length, minVal, maxVal)); break;
                case ArrayType.Sorted: GenerateFactory(new SortedArrayFactory(length, minVal, maxVal)); break;
                case ArrayType.PartialSorted: GenerateFactory(new PartialSortedArrayFactory(length, minVal, maxVal)); break;
            }

            return array;
        }

        public static string GetName(ArrayType type)
        {
            IArrayFactory factory = null;

            switch (type)
            {
                case ArrayType.Random: factory = new RandomArrayFactory(100); break;
                case ArrayType.Sorted: factory = new SortedArrayFactory(100); break;
                case ArrayType.PartialSorted: factory = new PartialSortedArrayFactory(100); break;
            }

            return factory?.Name ?? "";
        }

        /// <summary>
        /// Переставляет элементы в массиве
        /// </summary>
        /// <param name="array"></param>
        /// <param name="elements">Если elements=0, то выполняет перестановку всех элементов в массиве</param>
        public static void Shuffle(int[] array, int elements = 0)
        {
            Random rng = new Random();
            elements = elements == 0 ? array.Length : elements;
            int step = array.Length / elements;
            for (int i = array.Length - 1; i > 0; i=i-step)
            {
                int swapIndex = rng.Next(i + 1);
                if (swapIndex != i)
                {
                    int tmp = array[swapIndex];
                    array[swapIndex] = array[i];
                    array[i] = tmp;
                }
            }
        }
        private static void GenerateFactory(IArrayFactory factory)
        {
            array = factory.Generate();
        }
    }
}

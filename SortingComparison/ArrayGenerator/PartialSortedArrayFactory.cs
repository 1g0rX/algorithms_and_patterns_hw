﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayGenerator
{
    class PartialSortedArrayFactory : IArrayFactory
    {
        public string Name { get; set; } = "Частично отсортированный массив";
        public int ArrayLength { get; private set; }

        public int MinVal { get; private set; }

        public int MaxVal { get; private set; }

        readonly int parts;

        public PartialSortedArrayFactory(int arrayLength, int minVal = 0, int maxVal = 100, int parts = 2)
        {
            this.ArrayLength = arrayLength;
            this.MinVal = minVal;
            this.MaxVal = maxVal;
            this.parts = parts;
        }

        public int[] Generate()
        {
            int[] array = new int[ArrayLength];
            int p = 1;
            int i = 0;

            do
            {
                int length;
                if (p == parts)
                {
                    length = ArrayLength - i;
                }
                else length = ArrayLength / parts;
                foreach (int num in new SortedArrayFactory(length, MinVal, MaxVal).Generate())
                {
                    array[i++] = num;
                }

            }
            while (++p <= 2);

            return array;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayGenerator
{
    class SortedArrayFactory : IArrayFactory
    {
        public string Name { get; set; } = "Отсортированный массив";
        public int ArrayLength { get; private set; }

        public int MinVal { get; private set; }

        public int MaxVal { get; private set; }

        public SortedArrayFactory(int arrayLength, int minVal = 0, int maxVal = 100)
        {
            this.ArrayLength = arrayLength;
            this.MinVal = minVal;
            this.MaxVal = maxVal;
        }

        public int[] Generate()
        {
            int[] array = new RandomArrayFactory(ArrayLength, MinVal, MaxVal).Generate();
            Array.Sort(array);
            return array;
        }
    }
}

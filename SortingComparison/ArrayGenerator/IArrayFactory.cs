﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayGenerator
{
    interface IArrayFactory
    {
        string Name { get; }
        int ArrayLength { get; }
        int MinVal { get; }
        int MaxVal { get; }

        int[] Generate();
    }
}

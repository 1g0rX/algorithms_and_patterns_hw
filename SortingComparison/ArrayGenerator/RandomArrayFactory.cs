﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayGenerator
{
    class RandomArrayFactory : IArrayFactory
    {
        Random rnd = new Random();
        public string Name { get; set; } = "Массив случайных чисел";
        public int ArrayLength { get; private set; }

        public int MinVal { get; private set; }

        public int MaxVal { get; private set; }

        public RandomArrayFactory(int arrayLength, int minVal = 0, int maxVal = 100)
        {
            this.ArrayLength = arrayLength;
            this.MinVal = minVal;
            this.MaxVal = maxVal;
        }

        public int[] Generate()
        {
            int[] array = new int[ArrayLength];

            for (int i = 0; i < ArrayLength; i++)
            {
                array[i] = rnd.Next(MinVal, MaxVal);
            }

            return array;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SortingAlgorithms;

namespace SortAlgComparer.Model
{
    public class SortAlgDescription
    {
        public Algorithm Algorithm { get; set; }
        public string Name { get; set; }
        public string ShortName {  get; set; }
        public string Type {  get; set; }
        public string WorstTime {  get; set; }
        public string MiddleTime { get; set; }
        public string BestTime { get; set; }
    }
}

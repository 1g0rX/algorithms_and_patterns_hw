﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAlgComparer.Model
{
    class SortResult
    {
        [DisplayName("Алгоритм сортировки")]
        public string SortName { get; set; }
        [DisplayName("Тип массива")]
        public string ArrayType { get; set; }
        [DisplayName("Длина массива")]
        public int ArrayLength { get; set; }
        [DisplayName("Время сортировки, мс")]
        public long SortTime { get; set; }
        [DisplayName("Тактов")]
        public long Ticks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAlgComparer.Model
{
    class ArrayInfo 
    {
        public ArrayGenerator.ArrayType ArrayType { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public int MinVal { get; set; }
        public int MaxVal { get; set; }
    }
}

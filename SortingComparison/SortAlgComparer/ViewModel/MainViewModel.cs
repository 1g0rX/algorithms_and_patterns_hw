﻿using SortAlgComparer.Model;
using SortingAlgorithms;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Utils;


namespace SortAlgComparer.ViewModel
{
    class MainViewModel : PropertyChangedBase
    {
        #region Properties
        private ObservableCollection<SortAlgDescription> _algsDesc =
            new ObservableCollection<SortAlgDescription>();
        public ObservableCollection<SortAlgDescription> AlgsDesc
        {
            get => _algsDesc;
            set => SetField(ref _algsDesc, value);
        }

        private ObservableCollection<ArrayInfo> _arrayInfos =
            new ObservableCollection<ArrayInfo>();
        public ObservableCollection<ArrayInfo> ArrayInfos
        {
            get => _arrayInfos;
            set => SetField(ref _arrayInfos, value);
        }

        private SortAlgDescription _selectedAlg;
        public SortAlgDescription SelectedAlg
        {
            get => _selectedAlg;
            set => SetField(ref _selectedAlg, value);
        }

        private ArrayInfo _selectedArray;
        public ArrayInfo SelectedArray
        {
            get => _selectedArray;
            set => SetField(ref _selectedArray, value);
        }

        private ObservableCollection<SortResult> _sortResults =
            new ObservableCollection<SortResult>();
        public ObservableCollection<SortResult> SortResults
        {
            get => _sortResults;
            set => SetField(ref _sortResults, value);
        }

        private bool _isSortStart = false;
        public bool IsSortStart
        {
            get => _isSortStart;
            set => SetField(ref _isSortStart, value);
        }

        #endregion

        public MainViewModel()
        {
            FillSortAlgorithm();
            FillArrayTypes();
        }

        #region Methods
        void FillSortAlgorithm()
        {
            var algs = Enum.GetValues(typeof(Algorithm));
            foreach (var item in algs)
            {
                var alg = (Algorithm)item;
                ISort sort = SortFactory.Create(alg);
                AlgsDesc.Add(new SortAlgDescription
                {
                    Algorithm = alg,
                    Name = sort.Name,
                    ShortName = sort.ShortName,
                    Type = sort.Type,
                    WorstTime = sort.WorstTime,
                    MiddleTime = sort.MiddleTime,
                    BestTime = sort.BestTime
                });
            }
            AlgsDesc.OrderBy(a => a.Name);
            SelectedAlg = AlgsDesc.First();
        }

        void FillArrayTypes()
        {
            var arrays = Enum.GetValues(typeof(ArrayGenerator.ArrayType));
            foreach (var item in arrays)
            {
                var arrayType = (ArrayGenerator.ArrayType)item;
                string name = ArrayGenerator.GenerateArray.GetName(arrayType);
                ArrayInfos.Add(new ArrayInfo
                {
                    Name = name,
                    Length = 1000,
                    ArrayType = arrayType,
                    MinVal = 0,
                    MaxVal = 100
                });
            }
            ArrayInfos.OrderBy(a => a.Name);
            SelectedArray = ArrayInfos.First();
        }


        #endregion

        #region Commands
        private RelayCommand _startSort;
        public RelayCommand StartSort =>
            _startSort ?? (_startSort = new RelayCommand(async obj =>
           {
               ISort sort = SortFactory.Create(SelectedAlg.Algorithm);
               var array = ArrayGenerator.GenerateArray.Get(SelectedArray.ArrayType, SelectedArray.Length, SelectedArray.MinVal, SelectedArray.MaxVal);
               IsSortStart = true;
               Stopwatch sw = new Stopwatch();
               sw.Start();
               await Task.Run(() => sort.Sort(array));
               sw.Stop();
               IsSortStart = false;

               SortResults.Add(new SortResult
               {
                   SortName = sort.Name,
                   ArrayType = SelectedArray.Name,
                   ArrayLength = SelectedArray.Length,
                   SortTime = sw.ElapsedMilliseconds,
                   Ticks = sw.ElapsedTicks
               });

           }));

        private RelayCommand _sortResultsClear;
        public RelayCommand SortResultsClear =>
            _sortResultsClear ?? (_sortResultsClear = new RelayCommand(obj =>
            {
                SortResults.Clear();
            }));
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//Ссылка на алгоритм
//http://www.cyberforum.ru/csharp-beginners/thread231603.html
//Падения на отсортированном массиве и частично отсортированном
namespace SortingAlgorithms
{
    class QuickSort : ISort
    {
        public string Name { get; private set; }
        public string ShortName { get; private set; }
        public string Type { get; private set; }
        public string WorstTime { get; private set; }
        public string MiddleTime { get; private set; }
        public string BestTime { get; private set; }

        public QuickSort()
        {
            this.Name = "Быстрая сортировка";
            this.ShortName = "QuickSort";
            this.Type = "Вставками";
            this.WorstTime = "O(n2)";
            this.MiddleTime = "O(n log n)";
            this.BestTime = "O(n log n)";
        }

        public void Sort(int[] array)
        {
            if (array.Length == 0) return;
            QuickSortRecursive(ref array, 0, array.Length - 1);
        }

        private static void QuickSortRecursive(ref int[] data, int left, int right)
        {
            if (left < right)
            {
                int q = Partition(ref data, left, right);

                    QuickSortRecursive(ref data, left, q);
                    QuickSortRecursive(ref data, q + 1, right);
                
            }

        }

        private static int Partition(ref int[] a, int p, int r)
        {
            int x = a[p];
            int i = p - 1;
            int j = r + 1;
            while (true)
            {
                do
                {
                    j--;
                }
                while (a[j] > x);
                do
                {
                    i++;
                }
                while (a[i] < x);
                if (i < j)
                {
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
                else
                {
                    return j;
                }
            }
        }
    }
}

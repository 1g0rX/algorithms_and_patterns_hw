﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{ 
    public enum Algorithm
    {
        BubbleSort,
        CountingSort,
        FWIntoSort,
        IntroSort,
        QuickSort,
        ShellSort,
        TimSort
    }

    public class UnexpectedEnumValueException<T> : Exception
    {
        public UnexpectedEnumValueException(T value)
            : base("Value " + value + " of enum " + typeof(T).Name + " is not supported")
        {
        }
    }

    //паттерн фабричный метод
    public static class SortFactory
    {
        public static ISort Create(Algorithm algorithms)
        {
            switch (algorithms)
            {
                case Algorithm.BubbleSort: return new BubbleSort();
                case Algorithm.QuickSort: return new QuickSort();
                case Algorithm.CountingSort: return new CountingSort();
                case Algorithm.ShellSort: return new ShellSort();
                case Algorithm.TimSort: return new TimSort();
                case Algorithm.IntroSort: return new IntroSort();
                case Algorithm.FWIntoSort: return new FWIntroSort();
                default: throw new UnexpectedEnumValueException<Algorithm>(algorithms);
            }
        }
    }
}

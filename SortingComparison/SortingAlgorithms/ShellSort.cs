﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{

    //
    //https://programm.top/c-sharp/algorithm/array-sort/shell-sort/
    class ShellSort : ISort
    {
        public string Name { get; private set; }

        public string ShortName { get; private set; }
        public string Type { get; private set; }

        public string WorstTime { get; private set; }

        public string MiddleTime { get; private set; }

        public string BestTime { get; private set; }

        public ShellSort()
        {
            this.Name = "Сортировка Шелла";
            this.ShortName = "ShellSort";
            this.Type = "Вставками";
            this.WorstTime = "O(n^2)";
            this.MiddleTime = "Н/Д";
            this.BestTime = "O(n log^2 n)";
        }

        private void Swap(ref int a, ref int b)
        {
            var t = a;
            a = b;
            b = t;
        }

        public void Sort(int[] array)
        {
            //расстояние между элементами, которые сравниваются
            var d = array.Length / 2;
            while (d >= 1)
            {
                for (var i = d; i < array.Length; i++)
                {
                    var j = i;
                    while ((j >= d) && (array[j - d] > array[j]))
                    {
                        Swap(ref array[j], ref array[j - d]);
                        j = j - d;
                    }
                }

                d = d / 2;
            }
        }
    }
}

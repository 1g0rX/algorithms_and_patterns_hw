﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    public interface ISort
    {
        string Name { get; }
        string ShortName { get; }
        string Type { get; }
        string WorstTime { get; }
        string MiddleTime { get;}
        string BestTime { get; }

        void Sort(int[] array);
    }
}

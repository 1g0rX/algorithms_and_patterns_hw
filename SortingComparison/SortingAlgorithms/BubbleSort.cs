﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    //An optimized version of Bubble Sort 
    //https://www.geeksforgeeks.org/bubble-sort/
    class BubbleSort : ISort
    {
        public string Name { get; private set; }
        public string ShortName { get; private set; }
        public string Type { get; private set; }
        public string WorstTime { get; private set; }
        public string MiddleTime { get; private set; }
        public string BestTime { get; private set; }

        public BubbleSort()
        {
            this.Name = "Пузырьковая сортировка";
            this.ShortName = "Bubble";
            this.Type = "Обменные";
            this.WorstTime = "O(n2)";
            this.MiddleTime = "O(n2)";
            this.BestTime = "O(n)";
        }

        public void Sort(int[] array)
        {
            int temp;
            bool swapped;
            for (int i = 0; i < array.Length - 1; i++)
            {
                swapped = false;
                for (int j = 0; j < array.Length - i - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        swapped = true;
                        temp = array[j];
                        array[j] = array[j+1];
                        array[j+1] = temp;
                    }
                }
                if (!swapped) break;
            }
        }
    }
}

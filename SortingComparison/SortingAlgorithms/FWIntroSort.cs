﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    class FWIntroSort : ISort
    {
        public string Name { get; private set; }
        public string ShortName { get; private set; }
        public string Type { get; private set; }
        public string WorstTime { get; private set; }
        public string MiddleTime { get; private set; }
        public string BestTime { get; private set; }

        public FWIntroSort()
        {
            this.Name = "Интроспективная  сортировка (.NET FW)";
            this.ShortName = "FWIntroSort";
            this.Type = "Гибридный";
            this.WorstTime = "O(n log n)";
            this.MiddleTime = "O(n log n)";
            this.BestTime = "O(n log n)";
        }

        public void Sort(int[] array)
        {
            Array.Sort(array);
        }
    }
}

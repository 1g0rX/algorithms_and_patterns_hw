﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    //
    //https://www.geeksforgeeks.org/counting-sort/
    class CountingSort : ISort
    {
        public string Name { get; private set; }

        public string ShortName { get; private set; }
        public string Type { get; private set; }

        public string WorstTime { get; private set; }

        public string MiddleTime { get; private set; }

        public string BestTime { get; private set; }

        public CountingSort()
        {
            this.Name = "Сортировка подсчётом";
            this.ShortName = "CountingSort";
            this.Type = "Без сравнения";
            this.WorstTime = "O(n + k)";
            this.MiddleTime = "Н/Д";
            this.BestTime = "Н/Д";
        }
        
        public void Sort(int[] array)
        {
            int[] sortedArray = new int[array.Length];

            int minVal = array.Min();
            int maxVal = array.Max();

            int[] counts = new int[maxVal - minVal + 1];
            for (int i = 0; i < array.Length; i++)
            {
                counts[array[i] - minVal]++;
            }
            // recalculate
            counts[0]--;
            for (int i = 1; i < counts.Length; i++)
            {
                counts[i] += counts[i - 1];
            }

            // Sort the array
            for (int i = array.Length - 1; i >= 0; i--)
            {
                sortedArray[counts[array[i] - minVal]--] = array[i];
            }

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = sortedArray[i];
            }

        }
    }
}
